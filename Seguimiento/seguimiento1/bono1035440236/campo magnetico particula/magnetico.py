# desarrollo del problema realizar la grafica de la trayectoria de la particula en 3d
# conocemos el campo magnetico.
import numpy as np
import  matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits import mplot3d
class magnetico1():  # se define la clase. consideremos el problema para t=0
    def __init__(self,energia,angulo,masa,campo,tiempo,carga_electron): # definicion de parametros
        self.energia=energia # definimos la energia 
        self.angulo=angulo # el angulo en radianes
        self.masa=masa # definimos la masa
        self.campo=campo # definimos el campo
        self.tiempo=tiempo # definimos el tiempo
        self.carga_electron=carga_electron # definimos la carga del electron
        # importante ver el libro de sears zemansky volumen 2
        # estan las expresiones para aplicar al desarrollo del problema cap 27
        # E=1/2 m**2*v**2 despejar de aqui la velocidad
        self.velinit=np.sqrt(self.energia*2/self.masa) # velocidad inicial particula
        # el radio debe de ser igual a m*v_perpendicular/campo* q. ojo la la velocidad perpendicular es paralela al planoxy y perpendicular alz
        self.velperpendicular=self.velinit*np.sin(self.angulo) # velocidad perpendicular al eje z
        # el radio viene dada por la siguiente formula: (sears zmansky 27 vol2)
        self.radio_electron=(self.masa*self.velperpendicular)/(self.carga_electron*self.campo)
        # frecuencia angular ecuacion es vo/R de donde w= q*b/m
        self.angular_frequency=(self.carga_electron*self.campo)/self.masa
        # las respectivas velocidades en vox, voxy y voz vienen dadas de la siguiente manera
        self.velx=self.radio_electron*self.angular_frequency*np.cos(self.angular_frequency*self.tiempo)
        # la velocidad en voy
        self.vely=-self.radio_electron*self.angular_frequency*np.sin(self.angular_frequency*self.tiempo)
        # para la velocidad en z tenemos que:
        self.velz=self.velinit*self.tiempo
        # Ahora ya definida todas las variables procedemos a ver el comportamiento de los movimientos en x, y, z
        # las ecuaciones que vienen fueron sacadas de wikipedia "movimiento helicoidal"
        
    def posx(self): # note que es oscilatorio en x

        p_x=self.radio_electron*np.sin(self.angular_frequency*self.tiempo)    
        return p_x
    def posy(self): # oscilatorio en y.
        p_y=self.radio_electron*np.cos(self.angular_frequency*self.tiempo)
        return p_y

        # para la posicion en y crece en linea recta. si queremos conocer la condicion inicial hacemos t=0 y nos queda.
    def posz(self):
        z=self.velz*self.tiempo # crece linealmente
        return z


    def grafica(self):
        fig=plt.figure(figsize=(8,6))
        axes = plt.axes(projection="3d")
        axes.scatter3D(self.posx(),self.posy(),self.posz(),color="blue")
        axes.set_title("movimiento de una particula en campo magnetico",fontsize=14,fontweight="bold")
        axes.set_xlabel("X")
        axes.set_ylabel("Y")
        axes.set_zlabel("Z")
        plt.tight_layout()
        plt.savefig("trayectoria")
        plt.show()
        return
    
    


    


        




            






        
