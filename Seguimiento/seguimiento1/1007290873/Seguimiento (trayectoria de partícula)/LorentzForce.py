import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d

class lorentzForce():

    """
    Clase que permite describir el movmiento de una partícula cargada en 
    presencia de un campo magnetico uniforme solo con componente en el eje z
    """

    # Atributos de clase

    # Estos atributos (los 4 siguientes) están relacionados con la posición inicial de la partícula
    # y el ángulo phi en coordenadas esférica 

    # La trayectoría inicia en el orígen
    x0 = 0 # [m]
    y0 = 0 # [m]
    z0 = 0 # [m]

    # la velocidad no tiene componente en y
    phi0 = 0 # [rad]

    # Número de vueltas que se quieren observar en el gráfico
    folds_number = 5

    # Constructor

    def __init__(self, charge, magnetic_field, mass, kinetic_energy, angle):
        
        # Parámetros con los que se instancia la clase:

        # charge: carga de la partícula [C]
        # magnetic_field: campo magnético de inducción que interactúa con la partícula [T]
        # mass: masa de la partícula [kg]
        # kinetic_energy: energía cinética que posee la partícula [eV]
        # angle: ángulo que forma la partícula con el campo [grados]

        # Definición de atributos de instancia

        self.q = charge
        self.B = magnetic_field
        self.m = mass
        self.E_k = kinetic_energy * (1.6e-19) # Conversión de eV a J 
        self.theta = np.deg2rad(angle)
        # Magnitud de la velocidad
        self.v0 = np.sqrt(2 * (self.E_k / self.m)) 
        # Componentes de la velocidad
        self.v0_x = self.v0 * np.sin(self.theta) * np.cos(self.phi0)
        self.v0_y = self.v0 * np.sin(self.theta) * np.sin(self.phi0)
        self.v0_z = self.v0 * np.cos(self.theta)
        # Frecuencia angular
        self.w = (np.abs(self.q) * self.B) / self.m

    # Definición de métodos

    def x_position(self, t):
        w = float(self.w)
        if w == 0.0:
            # si la carga o el campo B son 0, entonces no hay interacción
            return  self.x0 + self.v0_x * t
        else:
            return self.x0 + (1 / self.w) * (self.v0_x * np.sin(self.w * t) + self.v0_y * (1 - np.cos(self.w * t)))

    def y_position(self, t):
        w = float(self.w)
        if w == 0.0:
            # si la carga o el campo B son 0, entonces no hay interacción
            return self.y0 + self.v0_y * t
        else:
            return self.y0 + (1 / self.w) * (self.v0_y * np.sin(self.w * t) + self.v0_x * (np.cos(self.w * t) - 1))

    def z_position(self, t):
        if self.theta == np.pi/2:
            # La partícula permanece en el plano xy en z = z0
            return self.z0
        else:
            return self.z0 + self.v0_z * t

    def graph_trajectory(self):
        
        w = float(self.w)
        if w == 0.0:
            # Caso en que q ó B es 0
            t = np.arange(0, 100, 0.01)
        else:
            # Array de tiempos, de 0 a 5 veces el periodo, con 1000 puntos 
            t = np.linspace(0, self.folds_number * ((2*np.pi) / self.w), 1000)

        # Arrays de posiciones
        x_positions = self.x_position(t)
        y_positions = self.y_position(t)
        z_positions = self.z_position(t)

        fig = plt.figure(figsize=(18,15))
        ax = plt.axes(projection = "3d")
        ax.plot3D(x_positions, y_positions, z_positions, "red")

        ax.set_title("Trayectoria de la partícula cargada en presencia del campo magnético", fontsize = 22)
        ax.set_xlabel("$x(t)$ (m)", fontsize = 16)
        ax.set_ylabel("$y(t)$ (m)", fontsize = 16)
        ax.set_zlabel("$z(t)$ (m)", fontsize = 16)

        fig.tight_layout()

        fig.savefig("lorentz_force_trajectory.png")





