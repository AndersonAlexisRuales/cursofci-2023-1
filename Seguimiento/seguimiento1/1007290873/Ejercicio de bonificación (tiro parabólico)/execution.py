import numpy as np
from MovimientoParabolico import movimientoParabolico, movimientoParabolicoConAire

if __name__ == "__main__":

    ######     Usando la clase movimientoParabolico     #######

    # Definimos los parametros para instanciar la clase movimientoParabolico, unidades en mks

    velocidad_inicial_1 = 30
    angulo_1 = 30
    posicion_inicial_x_1 = 0
    altura_inicial_1 = 20
    aceleracion_1 = -9.81
    nombre_grafica_1 = "movimientoParabolico.png"

    mov_parabolico_1 = movimientoParabolico(velocidad_inicial_1, angulo_1, posicion_inicial_x_1, altura_inicial_1, aceleracion_1, nombre_grafica_1)

    # Prueba imprimiendo algunos atributos y métodos

    print("Movimiento con clase movimientoParabólico:\n")

    print("Velocidad inicial en x y y: ({}, {}) m/s".format(mov_parabolico_1.vel_inicial_x, mov_parabolico_1.vel_inicial_y))

    print("Tiempo de la altura máxima: {} s".format(mov_parabolico_1.t_max()))

    print("Altura máxima: {} m".format(mov_parabolico_1.y_max()))

    print("Tiempo de vuelo: {} s\n".format(mov_parabolico_1.tiempo_vuelo()))

    # print que se puede usar como verificación:
    # print(mov_parabolico_1.y_posicion(mov_parabolico_1.tiempo_vuelo()))

    # Gráfica de este movimiento

    mov_parabolico_1.grafica_movimiento() 

    
    ######     Usando la subclase movimientoParabolicoConAire     #######

    # Definimos los parametros para instanciar la clase movimientoParabolicoConAire, unidades en mks

    velocidad_inicial_2 = 30
    angulo_2 = 45
    posicion_inicial_x_2 = 0
    altura_inicial_2 = 20
    aceleracion_y_2 = -9.81
    aceleracion_x_2 = -5
    nombre_grafica_2 = "movimientoParabolicoConAire.png"

    mov_parabolico_2 = movimientoParabolicoConAire(velocidad_inicial_2, angulo_2, posicion_inicial_x_2, altura_inicial_2, aceleracion_y_2, nombre_grafica_2, aceleracion_x_2)

    # Prueba imprimiendo algunos atributos y métodos

    print("\nMovimiento con clase movimientoParabolicoConAire:\n")

    print("Velocidad inicial en x y y: ({}, {}) m/s".format(mov_parabolico_2.vel_inicial_x, mov_parabolico_2.vel_inicial_y))

    print("Tiempo de la altura máxima: {} s".format(mov_parabolico_2.t_max()))

    print("Altura máxima: {} m".format(mov_parabolico_2.y_max()))

    print("Tiempo de vuelo: {} s".format(mov_parabolico_2.tiempo_vuelo()))

    # print que se puede usar como verificación:
    # print(mov_parabolico_2.y_posicion(mov_parabolico_2.tiempo_vuelo()))

    # Gráfica de este movimiento

    mov_parabolico_2.grafica_movimiento() 