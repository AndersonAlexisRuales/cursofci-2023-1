from tiro_parabolico_ejemplo import tiroParabolico

import matplotlib.pyplot as plt, numpy, math

#definimos nueva clase para el movimiento con viento en direccion x.
#esta clase hereda todos los atributos y metodos de tiroParabolico
#pero posee además el atributo "a" correspondiente a la aceleracion x.

class parabolicoViento(tiroParabolico):
    def __init__(self, velinit, alpha, g, h0, x0, a):
        super().__init__(velinit, alpha, g, h0, x0)

        print("inicializando clase parabolicoViento")

        self.a=a

    #su caracteristica es el metodo posX_viento, que calcula la posicion
    #en x como un movimeinto acelerado uniforme.
    def posX_viento(self):
        posx = [self.x0 + i * self.velX() + 0.5 * self.a * i ** 2 for i in self.arrTime()]
        return posx
    
    #graficamos la trayectoria
    def figParabolico_viento(self):
        plt.figure(figsize=(10, 8))
        plt.title("Trayectoria movimiento parabolico con viento en x")
        plt.xlabel("x[m]")
        plt.ylabel("y[m]")
        plt.plot(self.posX_viento(), self.posY())
        plt.text(self.x0, 0, 'a = {} m/s2\n g = {} m/s2\nV0 = {} m/s \nt_vuelo = {:.1f} s'.format(self.a,self.g,self.velinit,self.tMaxVuelo()) , bbox=dict(facecolor='red', alpha=0.5))
        plt.savefig("parabolicoViento.png")
