
from parabolico_viento import parabolicoViento

if __name__=="__main__":
    print("***************IMPORTANTE****************************************")
    print("el sistema de referencia es positivo hacia arriba y a la derecha")
    print("*****************************************************************")

    velinit=20 #magnitud de la velocidad inicial
    alpha=45 #angulo de inclinacion
    g=-9.8 #aceleracion gravitacional
    h0=40 #altura inicial
    x0=50 #posicion horizontal inicial
    a=-5 #aceleracion del viento (+ a la derecha)

    tirop=parabolicoViento(velinit,alpha,g,h0,x0,a)
    tirop.figParabolico_viento()


 
