import numpy as np
from movimiento import movimiento
import matplotlib.pyplot as plt

if __name__ == "__main__":

# valores iniciales

    x0 = 0                        # posición inician en x
    y0 = 0                        # posición inician en y
    z0 = 0                        # posición inician en z
    E = 18.6 * 1.602e-19          # energía cinética
    theta = 30                    # ángulo entre el campo magnético y la velocidad
    phi = 0                       # ángulo azimutal 
    B = 6e-4                      # campo magnético
    q = 1.602e-19                 #  carga del electrón
    m = 9.11e-31                  # masa del electrón

    trayectoria = movimiento(x0, y0, z0, E, theta, phi, B, q, m)

    #print("la velocidad en x es {}, en y es {}, y en z es {}".format(trayectoria.vx(), trayectoria.vy(), trayectoria.vz()))
    plt.show(trayectoria.graficar())