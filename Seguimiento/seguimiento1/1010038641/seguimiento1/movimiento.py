import numpy as np
import matplotlib.pyplot as plt


class movimiento():
    # constructor
    def __init__(self, x0, y0, z0, E, theta, phi, B, q, m):

        # atributos de la clase
        self.theta = np.deg2rad(theta)
        self.phi = np.deg2rad(phi)
        self.B = B
        self.q = q
        self.E  = E
        self.m = m
        self.x0 = x0
        self.y0 = y0
        self.z0 =  z0

    # métodos 

    # velocidad en términos de la energía cinética
    def v(self):
        return np.sqrt(2 * self.E /self.m )
    

    # frecuencia de ciclotrón (omega)
    def w(self):
        return self.B * self.q / self.m
    
    # Periodo
    def T(self):
        return 2*np.pi/self.w()

    # array de tiempo
    def t(self):
        n = 7 # periodos que queremos considerar
        return np.linspace(0, n*self.T(), 1000)
    
    # velocidad en x
    def vx(self):
        return self.v() * np.sin(self.theta) * np.cos(self.phi)
    
    # velocidad en y
    def vy(self):
        return self.v() * np.sin(self.theta) * np.sin(self.phi)
    
    # velocidad en z
    def vz(self):
        if self.theta == np.pi/2: # caso particular para cuando la velocidad está en el plano xy
            return 0
        else:
            return self.v() * np.cos(self.theta) 
    
    # coordenadas 
    def x(self):
        return (self.vx() * np.sin(self.w() * self.t()) - self.vy() * np.cos(self.w() * self.t()) + self.vy())/self.w() + self.x0

    def y(self):
        return (self.vx() * np.cos(self.w() * self.t()) + self.vy() * np.sin(self.w() * self.t())- self.vx())/self.w() + self.y0
    
    def z(self):
        return self.vz() * self.t()

    # graficar las coordenadas 

    def graficar(self):
        #f = plt.figure()
        #plt.title('Movimiento helicoidal de un electrón en un campo magnético')
        ax = plt.axes(projection='3d')
        ax.scatter(self.x()[0], self.y()[0], self.z()[0], color = 'red', label = 'posición inicial')
        ax.plot3D(self.x(), self.y(), self.z(), color = 'darkblue', label = 'trayectoria')
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        ax.grid()
        ax.legend()
        plt.show()
        plt.savefig('trayectoria.png')
        return 