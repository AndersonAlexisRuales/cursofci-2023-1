from Movimientos2D import *
from ComparacionMovimientos import *


if __name__=='__main__':
    print('Name: {}'.format(__name__))

    # Agrege condiciones iniciales de su preferencia 
    x_0 = 0.
    y_0 = 10.
    v_0 = 10.
    angle = 45. # en grados
    a_y = -9.8 # Aceleración en y
    a_x = -1 # Aceleración en x
    
    # -----Movimiento Parabólico-----
    # Particula = MovimientoParabolico(x_0,y_0,v_0,angle,g = a_y) # Inicializando
    # print(Particula.info()) # Imprime información de la trayectoria
    # Particula.Graf() # Gŕafica de la trayectoria

    # -----Movimiento del Proyectil-----
    # Particula = MovimientoProyectil(x_0,y_0,v_0,angle,g = a_y,a_x = a_x) # Inicializando
    # print(Particula.info()) # Imprime información de la trayectoria
    # Particula.Graf() # Gŕafica de la trayectoria

    # -----Comparar ambos movimientos: -----
    CompararMovimiento(x_0,y_0,v_0,angle,g = a_y,a_x = a_x) # Vease Plot_Movimiento.png



